package nbcp

import doms.*
import org.slf4j.LoggerFactory
import org.springframework.boot.Banner
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.time.Duration
import java.time.LocalDateTime
import nbcp.comm.*;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import java.util.concurrent.Executors

//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession

@SpringBootApplication
class MainApplication : CommandLineRunner {
    companion object {
        val logger = LoggerFactory.getLogger(MainApplication::class.java)
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            //disabled banner, don't want to see the spring logo
            val app = SpringApplication(MainApplication::class.java)
            app.setBannerMode(Banner.Mode.OFF)
            app.run(*args)
        }
    }

    // Put your logic here.
    @Throws(Exception::class)
    override fun run(vararg args: String) {
        var startAt = LocalDateTime.now();
        try {
            exec(*args);
        } catch (e: Exception) {
            e.printStackTrace();
            System.exit(1)
            return;
        }
    }

    fun exec(vararg args: String) {

        val text = html(lang = "zh") {
            head("abc") {
                script{"alert('123')"}
            }
            body {
                h1 { "Hello" }

                table {
                    set("aa","bb")
                    thead {
                        tr {
                            th { "name" }
                            th { "age" }
                        }
                    }
                    tbody {
                        tr {
                            td { "yitian" }
                            td { "24" }
                        }
                        tr {
                            td { "liu6" }
                            td { "16" }
                        }
                    }
                }
                p { "This is some words" }
            }
        }
        println("html构造器使用实例:\n$text")

    }
}

