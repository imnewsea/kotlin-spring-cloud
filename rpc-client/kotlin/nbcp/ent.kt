package nbcp

interface ITestTalk {
    fun say(content: String): String
}
