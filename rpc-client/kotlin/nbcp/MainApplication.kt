package nbcp

import org.slf4j.LoggerFactory
import org.springframework.boot.Banner
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.time.Duration
import java.time.LocalDateTime
import nbcp.comm.*;
import org.smartboot.socket.transport.AioQuickClient
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import nbcp.rpc.RpcConsumerProcessor
import nbcp.rpc.RpcProtocol
import java.util.concurrent.Executors

//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession

class MainApplication {
    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            var serverIp = args.getOrElse(0) { "localhost" }
            var serverPort = args.getOrNull(1).AsInt(8888)

            val rpcConsumerProcessor = RpcConsumerProcessor()
            val consumer = AioQuickClient<ByteArray>(serverIp, serverPort, RpcProtocol(), rpcConsumerProcessor)
            var session = consumer.start()

            println("已连接："+ session.localAddress.port + " --> " + session.remoteAddress )

            val demoApi = rpcConsumerProcessor.getObject(ITestTalk::class.java)

            val pool = Executors.newCachedThreadPool()
            pool.execute { System.out.println(demoApi.say("abc")) }
            pool.execute { System.out.println(demoApi.say("def")) }
        }
    }
}
