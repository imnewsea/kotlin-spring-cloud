# 商城

## 正则

    搜索：
    .set\(mor.\w+.\w+.([^,]+),(.*)\)
    
    替换：
    .set\{it.$1 to $2}

## 提取依赖
```
mvn dependency:copy-dependencies -DoutputDirectory=C:/lib  -DexcludeGroupIds=dev8 -f 各项目名
```

( 注意去除单独打包后包含的包，即去除本项目的包。 -DexcludeGroupIds=dev8)
这就是每个项目的依赖包。
## gateway 项目，会多出一些包。
## corp,shop 两个项目，会多抽取出无用包：删除以下：
* spring-cloud-gateway-core

否则出错。 

下面包，可删可不删。
* spring-cloud-starter-gateway


## 编译底层Jar包
```
python build_base.py
```

## 编译 corp Jar包
```
cd corp
mvn clean package -Dmaven.test.skip=true
```

或者：
```
mvj-jar.bat -f corp
```

# 新项目

实施新项目时， 克隆本项目，执行以下命令修改 groupId 
```
python change_pom.py -g newGroupId 
```

# 线上配置文件

radmin.sh
```
java -Xmx250m  -Dfile.encoding=utf-8 \
  -jar admin-api-1.0.1.jar \
  --name=edu_admin \
  --spring.profiles.active=server \
  --server.port=9111 \
  --server.filter.allowOrigins=dev8.cn \
  --server.redisIp=172.18.0.20   \
  --server.mongodbIp=172.18.0.50 \
  --server.dbIp=localhost \
  --server.upload.path=/opt/edu_report/file \
  --server.upload.host=http://dev8.cn:9130
```

