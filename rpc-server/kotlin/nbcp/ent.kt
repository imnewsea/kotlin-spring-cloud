package nbcp

interface ITestTalk {
    fun say(content: String): String
}

class TestTalk : ITestTalk {
    override fun say(content: String): String {
        println(content)
        return content + "!";
    }
}