package nbcp

import org.slf4j.LoggerFactory
import org.springframework.boot.Banner
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.time.Duration
import java.time.LocalDateTime
import nbcp.comm.*;
import nbcp.rpc.RpcProtocol
import nbcp.rpc.RpcProviderProcessor
import org.smartboot.socket.transport.AioQuickServer
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration

//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession

@SpringBootApplication
class MainApplication : CommandLineRunner {
    companion object {
        val logger = LoggerFactory.getLogger(MainApplication::class.java)
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            //disabled banner, don't want to see the spring logo
            val app = SpringApplication(MainApplication::class.java)
            app.setBannerMode(Banner.Mode.OFF)
            app.run(*args)
        }
    }

    // Put your logic here.
    @Throws(Exception::class)
    override fun run(vararg args: String) {
        var startAt = LocalDateTime.now();
        try {
            exec(*args);
        } catch (e: Exception) {
            e.printStackTrace();
            System.exit(1)
            return;
        }
    }

    fun exec(vararg args: String) {
        var port = 8888;
        val rpcProviderProcessor = RpcProviderProcessor()
        val server = AioQuickServer(port, RpcProtocol(), rpcProviderProcessor)
        server.start()
        println("smart-socket-server 已启动: ${port}")
        rpcProviderProcessor.publishService(ITestTalk::class.java, TestTalk())
    }
}
