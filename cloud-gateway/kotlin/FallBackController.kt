package nbcp

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono


/**
 * Created by yuxh on 2019/4/28
 */
//@RestController
//class FallBackController{
//    @RequestMapping(value = ["/fallback"])
//    fun fallBackController(response:HttpServletResponse): Map<String, String> {
//        response.status = 500;
//        val res = LinkedHashMap<String,String>()
//        res.put("msg", "fallback")
//        return res
//    }
//}

@RestController
class FallBackController {
    @RequestMapping("/fallback", method = arrayOf(RequestMethod.GET, RequestMethod.POST))
    fun fallBackController(): Mono<Map<String, String>> {
        val res = LinkedHashMap<String, String>()
        res.put("msg", "fallback")

        return Mono.just(res)
    }
}