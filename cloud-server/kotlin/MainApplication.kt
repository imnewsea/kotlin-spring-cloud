package nbcp

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer
import org.springframework.stereotype.Service

/**
 * 使用Eureka做服务发现.
 * @author eacdy
 */
@SpringBootApplication
@EnableEurekaServer
open class MainApplication {
    companion object {
        val logger = LoggerFactory.getLogger(this::class.java.declaringClass)
    }
}

fun main(args: Array<String>) {
    var context = SpringApplication.run(MainApplication::class.java, *args) as ServletWebServerApplicationContext

    MainApplication.logger.trace("""
        ================================================
             ${context.debugServerInfo}
        ================================================
""")
}


val ServletWebServerApplicationContext.debugServerInfo: String
    get() {
        var serverName = this.servletContext!!.serverInfo
        var applicationName = this.environment.getProperty("spring.application.name")
        var version = this.environment.activeProfiles.joinToString(",")
        var cloudVersion = this.environment.getProperty("spring.cloud.config.profile") ?: ""
        var port = this.environment.getProperty("server.port")

        if (cloudVersion.isNotEmpty()) {
            version += "-cloud-" + cloudVersion
        }

        return "${serverName}:${port} -- ${applicationName}:${version}"
    }