# cloud 部署

* 在两个docker中分别运行 center 集群

 
    java -Xmx150m -jar  shop-center*.jar --spring.profiles.active=dn2
    java -Xmx150m -jar  shop-center*.jar --spring.profiles.active=dn3
    
* 在另外两个docker中分别运行 api 提供者。


    java -Xmx150m -jar shop-api-3.0.0.jar --spring.profiles.active=local --shop.mq.consumer=false --server.port=3061
    java -Xmx150m -jar shop-api-3.0.0.jar --spring.profiles.active=local --shop.mq.consumer=false --server.port=3062

* 在docker中运行 gateway , 多个 gateway 用 nginx 做负载


    java -Xmx150m -jar shop-gateway-3.0.0.jar
    
# 配置
    
    defaultZone 配置到任何一个注册中心都可以利用注册中心的集群。即写多个和写一个是一样的。